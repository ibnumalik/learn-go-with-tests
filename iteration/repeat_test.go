package iteration

import (
	"fmt"
	"testing"
)

func TestRepeat(t *testing.T) {
	assertCorrectRepeat := func(t testing.TB, repeated, expected string) {
		t.Helper()
		if repeated != expected {
			t.Errorf("expect %q but got %q", expected, repeated)
		}
	}

	t.Run("repeat 'a' 5 times", func(t *testing.T) {
		repeated := Repeat("a", 5)
		expected := "aaaaa"
		assertCorrectRepeat(t, repeated, expected)
	})

	t.Run("repeat 'b' 3 times", func(t *testing.T) {
		repeated := Repeat("b", 3)
		expected := "bbb"
		assertCorrectRepeat(t, repeated, expected)
	})

	t.Run("repeat 'b' 3 times", func(t *testing.T) {
		banana := "ba" + Repeat("na", 2)
		expected := "banana"
		assertCorrectRepeat(t, banana, expected)
	})
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 5)
	}
}

func ExampleRepeat() {
	repeat := Repeat("c", 7)
	fmt.Println(repeat)
	// Output: ccccccc
}
