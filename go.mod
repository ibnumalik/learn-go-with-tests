module github.com/ibnumalik/learn-go-with-tdd

go 1.15

require (
	github.com/kisielk/errcheck v1.5.0 // indirect
	golang.org/x/mod v0.4.1 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/tools v0.1.0 // indirect
)
